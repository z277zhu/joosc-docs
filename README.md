# Documents for Joos Compiler 
1. joos compiler parsing.pdf discusses the project structure, technical stacks and technical challenges we encountered in parsing
2. joos compiler static checking.pdf discusses the overall pipeline of static checking, including building Type Environment, Hierarchy Checking, Type Checking and Unreachable Statement Checking
3. joos compiler back-end.pdf describes in detail about our design and implementation of the compiler's back-end